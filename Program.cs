﻿using System;

// Abstract Product: Fruit
public interface IFruit
{
    void Display();
}

// Concrete Products
public class Apple : IFruit
{
    public void Display()
    {
        Console.WriteLine("This is an Apple.");
    }
}

public class Banana : IFruit
{
    public void Display()
    {
        Console.WriteLine("This is a Banana.");
    }
}

// Abstract Factory
public interface IFruitFactory
{
    IFruit CreateFruit();
}

// Concrete Factories
public class AppleFactory : IFruitFactory
{
    public IFruit CreateFruit()
    {
        return new Apple();
    }
}

public class BananaFactory : IFruitFactory
{
    public IFruit CreateFruit()
    {
        return new Banana();
    }
}

public class Program
{
    public static void Main()
    {
        // Create an Apple using the AppleFactory
        IFruitFactory appleFactory = new AppleFactory();
        IFruit apple = appleFactory.CreateFruit();
        apple.Display();

        // Create a Banana using the BananaFactory
        IFruitFactory bananaFactory = new BananaFactory();
        IFruit banana = bananaFactory.CreateFruit();
        banana.Display();
    }
}
